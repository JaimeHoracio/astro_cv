/** @type {import('tailwindcss').Config} */
module.exports = {
	content: [
		'./components/**/*.{html,js}',
		'./pages/**/*.{html,js}',
		'./src/**/*.{astro,html,js,jsx,md,mdx,svelte,ts,tsx,vue}',
	],
	theme: {
		screens: {
			sm: '280px',
			md: '66px',
			lg: '970px',
			xl: '1440px'
		},
		fontFamily: {
			sans: ['Graphik', 'sans-serif'],
			serif: ['Merriweather', 'serif']
		},
		constainer: {
			center: true,
		},
		extend: {
			colors: {
				'fondo-color': '#fff4f4',
			},
		},
	},
	plugins: [require("rippleui")],
}
